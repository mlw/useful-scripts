#!/bin/sh

# reads wayland clipboard primary and uses tofi to show a menu
# to choose which program to pass it to. (inspiration: luke smith)
# requires: wl-clipboard, foot(default terminal), notify-send
# evince, librewolf, mpv, imv

show_error() {
  notify-send "$1"
  >&2 echo "$1"
  exit 1
}

terminal="foot"
primary="$(wl-paste --primary --no-newline)"
if [ -z "$primary" ]; then 
  >&2 echo "primary clipboard is empty or other error, trying secondary clipboard.."
  notify-send "trying secondary clipboard"
  primary="$(wl-paste --no-newline)" || show_error "primary and secondary clipboard is empty or other error"
fi

notify-send "Clipboard content: $primary"

input="$(printf "librewolf\nlibrewolf(search)\nmpv\nlynx\nlagrange(url or search)\nman\nman(pdf)\nrun(term)\nrun\nnvim\nimv\n" | tofi --fuzzy-match true --prompt-text "choose program: ")"
input="$(echo "$input" | tr -d '\n')"

if [ -z "$input" ]; then
  >&2 echo "Invalid input"
  notify-send "Invalid input"
  exit 1
fi


case "$input" in
  "mpv")
    mpv "$primary" || show_error "error running mpv" ;;
  "librewolf")
    librewolf-wayland "$primary" || show_error "error running librewolf" ;;
  "librewolf(search)")
    librewolf-wayland "https://duckduckgo.com/?q=$primary" || show_error "error running librewolf" ;;
  "lagrange(url or search)")
    lagrange --url-or-search "$primary" || show_error "error running lagrange" ;;
  "lynx")
    "$terminal" sh -c "lynx $primary" || show_error "error running lynx" ;;
  "run(term)")
    "$terminal" sh -c "$primary" || show_error "error running the program" ;;
  "run")
    "$primary" || show_error "error running the program" ;;
  "nvim")
    "$terminal" sh -c "nvim $primary" || show_error "error running nvim" ;;
  "man")
    "$terminal" sh -c "man $primary" || show_error "error running man" ;;
  "man(pdf)")
    (man -Tpdf "$primary" > "/tmp/plumber-$primary.tmp") || show_error "error running man"
    evince "/tmp/plumber-$primary.tmp"
    rm "/tmp/plumber-$primary.tmp"
    ;;
  "imv")
    imv "$primary" || show_error "error running imv" ;;
  *)
    show_error "there was an error(check the script)"
esac

# only clear primary because others would be inconvenient
wl-copy --primary --clear
