# useful-scripts

Some useful scripts I wrote/got from the internet

* bookmarks, bookmpv, wpicker - inspired by [this video](https://www.youtube.com/watch?v=d_11QaTlf1I)
* plumber - inspired by [this video](https://www.youtube.com/watch?v=RlMxbQmMz_4)
* webm2mp3, mp32ogg, mp42webm - converting multimedia file formats with ffmpeg
* screenshot - screenshots with grim, slurp and swappy
* uploadscr - upload the screenshot to 0x0.st
